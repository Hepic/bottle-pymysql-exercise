<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="static/update_artist.css" />
    </head>
    
    <body>
        <div id='container'>
            <h1>Update Artist Information</h1>
            <hr>
            <div id='inputs'>
                <form action='/do_update_artist' method='post'>
                    <p>
                        <label for='name'>Name</label>
                        <input type='text' id='name' name='name' value='{{name}}'/>
                    </p>
                    <p>
                        <label for='surname'>Surname</label>
                        <input type='text' id='surname' name='surname' value='{{surname}}'/>
                    </p>
                    <p>
                        <label for='birth'>Birth</label>
                        <input type='text' id='birth' name='birth' value='{{birth}}'/>
                    </p>
                    <input type='hidden' name='id' value='{{Id}}'/>
                    <p>
                        <input type='submit' value='Update Information' />
                    </p>
                </form>
            </div>
            <hr>
        </div>
    </body>
</html>
