from bottle import route, run, post, get, request, redirect, template, static_file
import pymysql.cursors


@route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='static/')


@route('/')
def index():
    return template('index.tpl')


@get('/update_search_artist')
def update_search():
    return template('update_search_artist.tpl')


@post('/search_artist')
def search():
    name = request.forms.get('name')
    surname = request.forms.get('surname')
    birth_from = request.forms.get('birth_from')
    birth_to = request.forms.get('birth_to')
    Type = request.forms.get('type')
    
    conn = pymysql.connect(host='localhost', port=3306, user='singer', passwd='singer', db='songs', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
    
    try:
        with conn.cursor() as cursor:
            if(Type == 'singer'):
                query = '''SELECT * FROM kalitexnis INNER JOIN singer_prod WHERE ar_taut=tragoudistis AND 
                           etos_gen BETWEEN %d AND %d GROUP BY ar_taut;''' % (int(birth_from), int(birth_to))
            
            elif(Type == 'song_writer'):
                query = '''SELECT * FROM kalitexnis INNER JOIN tragoudi WHERE ar_taut=stixourgos AND 
                           etos_gen BETWEEN %d AND %d GROUP BY ar_taut;''' % (int(birth_from), int(birth_to))
            
            elif(Type == 'composer'):
                query = '''SELECT * FROM kalitexnis INNER JOIN tragoudi WHERE ar_taut=sinthetis AND 
                           etos_gen BETWEEN %d AND %d GROUP BY ar_taut;''' % (int(birth_from), int(birth_to))
            
            cursor.execute(query)
            result = cursor.fetchall()

            urls = []
            for elem in result:
                url = 'http://localhost:9090/update_artist?'
                url += 'id=' + elem['ar_taut'] + '&name=' + elem['onoma'] + '&surname=' + elem['epitheto'] + '&birth=' + str(elem['etos_gen'])
                urls.append(url)

            return template('search_artist.tpl',  result=result, urls=urls)

    finally:
        conn.close()


@get('/update_artist')
def update():
    Id = request.query.get('id')
    name = request.query.get('name')
    surname = request.query.get('surname')
    birth = request.query.get('birth')

    return template('update_artist.tpl', Id=Id, name=name, surname=surname, birth=birth)


@post('/do_update_artist')
def do_update():
    Id = request.forms.get('id')
    name = request.forms.get('name')
    surname = request.forms.get('surname')
    birth = request.forms.get('birth')

    conn = pymysql.connect(host='localhost', port=3306, user='singer', passwd='singer', db='songs', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
 
    try:
        with conn.cursor() as cursor:
            query = 'UPDATE kalitexnis SET onoma="%s", epitheto="%s", etos_gen=%d WHERE ar_taut="%s";' % (name, surname, int(birth), Id)            
            cursor.execute(query)
        
        conn.commit()

    finally:
        conn.close()

    return redirect('/')


if(__name__ == '__main__'):
    run(host='localhost', port=9090, debug=True, reloader=True) #change port to 9090 and change debug to False
