<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="static/search_artist.css" />
    </head>
    
    <body>
        <div id='container'>
            <h1>View Artist Results</h1>
            <hr>
            <table>
                <tr>
                    <th>National ID</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Birth Year</th>
                    <th>Edit?</th>
                </tr>
                % i = 0
                % for elem in result:
                    <tr>
                        <td>X {{elem['ar_taut']}}</td> 
                        <td>{{elem['onoma']}}</td>
                        <td>{{elem['epitheto']}}</td>
                        <td>{{elem['etos_gen']}}</td>
                        <td><a href={{urls[i]}}>Edit Me!</a></td>
                    </tr>
                % i += 1
                %end
            </table>
            <hr>
        </div>
    </body>
</html>
