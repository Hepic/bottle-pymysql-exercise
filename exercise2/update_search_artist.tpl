<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="static/update_search_artist.css" />
    </head>
    
    <body>
        <div id='container'>
            <form action='/search_artist' method='post'>
                <p>
                    <label for='name'>Name </label>
                    <input type='text' id='name' name='name'/>
                </p>
                <p>
                    <label for='surname'>Surname </label>
                    <input type='text' id='surname' name='surname'/>
                </p>
                <p>
                    <label for='birth_from'>Birth Year - From </label>
                    <input type='text' id='birth_from' name='birth_from'/>
                </p>
                <p>
                    <label for='birth_to'>Birth Year - To </label>
                    <input type='text' id='birth_to' name='birth_to'/>
                </p>
                
                <p>
                    <div id='type'>
                        <div id='descr'>
                            <label>Type</label>
                        </div>
                        <div id='choices'>
                            <p><input type='radio' name='type' value='singer' checked>Singer</p>
                            <p><input type='radio' name='type' value='song_writer'>Song Writer</p>
                            <p><input type='radio' name='type' value='composer'>Composer</p>
                        </div>
                    </div>
                </p>
                </p>
                    <input type='submit' value='submit' />
                </p>
            </form>
        </div>
    </body>
</html>
